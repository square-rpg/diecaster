enablePlugins(JavaAppPackaging)

import sbt.Resolver

val dottyVersion = "2.13.4"

lazy val root = project
  .in(file("."))
  .settings(
    name := "diecaster",
    herokuAppName in Compile := "diecaster",
    herokuProcessTypes := Map(
      "worker" -> "target/universal/stage/bin/diecaster"
    ),
    
    version := "0.0.1",

    mainClass in Compile := Some("Main"),

    resolvers += Resolver.JCenterRepository,

    scalaVersion := dottyVersion,

    libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test",

    libraryDependencies += "net.katsstuff" %% "ackcord" % "0.17.1" withSources(),
    libraryDependencies += "com.lihaoyi" %% "cask" % "0.7.7" withSources(),

    libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
    libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3", 
  )
