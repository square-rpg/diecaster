import org.junit.Test
import org.junit.Assert._

class DiceTest {
  import Dice._

  @Test
  def printResult() = {

    val r = Dice.Result(List(List(3,5,6,6,2), List(2,6), List(4)), successes = 5, cutoff = 4)
    
    val expected =
      """|**Wurf mit 5 Würfeli**
         |**1.** 3, 5, 6, 6, 2
         |**2.** 2, 6
         |**3.** 4
         |**Erfolge (≥4):** 5""".stripMargin

    assertEquals(Dice.printResult(r), expected)
  }

}
