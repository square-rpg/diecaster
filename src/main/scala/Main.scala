import com.typesafe.scalalogging.LazyLogging

object Main extends LazyLogging {

  def main(args: Array[String]): Unit = {
    val discordApiToken = sys.env.getOrElse("DISCORD_API_KEY", sys.error("DISCORD_API_KEY environment variable not set"))

    val httpPort = 
      sys.env.get("PORT")
        .toRight("PORT environment variable not set")
        .flatMap(port => port.toIntOption.toRight("PORT environment variable value is not a valid integer value"))
        .fold(sys.error, identity)

    logger.info(s"http.port is $httpPort")    

    val bot = new DiecasterBot(DiecasterBot.DiscordApiToken(discordApiToken))

    val webserver = new Webserver(httpPort)
    
    bot.run()
    webserver.run()

  }
}
