
object Dice {
  case class Result(cast: List[List[Int]], successes: Int, cutoff: Int, note: Option[String] = None)

  private val Sides: Int = 6

  def cast(numberOfDice: Int, cutoff: Int = 4, note: Option[String] = None): Dice.Result = {

    val random = new java.util.Random()
    val buffer = scala.collection.mutable.ArrayBuffer[List[Int]]()
    var remainingDice = numberOfDice

    while (remainingDice > 0) {
      val cast = List.fill(remainingDice)(Math.abs(random.nextInt % Sides) + 1)
      buffer += cast
      remainingDice = cast.count(_ == Sides)
    }

    Dice.Result(buffer.toList, buffer.flatten.count(_ >= cutoff), cutoff, note)
  }

  def printResult(result: Dice.Result): String = {

    val Result(casts, successes, limit, note) = result
    val initialNumberOfDice = casts.headOption.map(_.size).getOrElse(0)
    val header = s"**Wurf mit $initialNumberOfDice Würfeli**${note.map(s => s" ($s)").getOrElse("")}"

    val castsMessage = casts.zipWithIndex.map {
      case (eyes, index) =>
        s"**${index + 1}.** " + eyes.mkString(", ")
    }.mkString("\n")

    val resultMessage = s"**Erfolge (≥$limit):** $successes"

    List(header, castsMessage, resultMessage).mkString("\n")
  }
}