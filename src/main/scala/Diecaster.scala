import ackcord._
import ackcord.data._
import ackcord.commands._
import com.typesafe.scalalogging.LazyLogging

class DiecasterBot(token: DiecasterBot.DiscordApiToken) extends LazyLogging {

  class Commands(requests: Requests) extends CommandController(requests) {

    import ackcord.syntax._

    val messageParser =
      implicitly[MessageParser[Int]]
        .andThen(MessageParser.optional[Int])

    val throwDice =
      Command
        .named(Seq("!"), Seq("t", "throw"))
        .parsing(messageParser)
        .withRequest { message =>
          val ((numberOfDice), cutoff) = message.parsed
          val result = Dice.cast(numberOfDice, cutoff.getOrElse(4), Some(message.user.username))

          message.textChannel.sendMessage(Dice.printResult(result))
        }
  }

  def run() = {

    val clientSettings = ClientSettings(token.value)

    import clientSettings.executionContext

    for {
      client <- clientSettings.createClient()
    } yield {
      val diecasterBot = new Commands(client.requests)

      client.commands.runNewNamedCommand(diecasterBot.throwDice)

      client.onEventSideEffectsIgnore {
        case APIMessage.Ready(_) => logger.info("Now ready to process commands")
      }

      client.login()
    }
  }
}

object DiecasterBot {
  case class DiscordApiToken(value: String)
}
