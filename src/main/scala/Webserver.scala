import io.undertow.Undertow
import com.typesafe.scalalogging.LazyLogging

class Webserver(port: Int) extends cask.MainRoutes with LazyLogging {

  override val host: String = "0.0.0.0"

  @cask.get("/")
  def state(): String = "running"

  def run(): Unit = {
    initialize()

    val server = Undertow.builder
      .addHttpListener(port, host)
      .setHandler(defaultHandler)
      .build

    server.start()

    logger.info(s"Webserver listening on $host:$port")
  }
}