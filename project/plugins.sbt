//addSbtPlugin("ch.epfl.lamp" % "sbt-dotty" % "0.4.6")
addSbtPlugin("ch.epfl.scala" % "sbt-bloop" % "1.4.6")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.7.6")
addSbtPlugin("com.heroku" % "sbt-heroku" % "2.1.4")